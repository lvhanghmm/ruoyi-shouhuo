import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listAuth(query) {
    return request({
        url: '/system/auth/list',
        method: 'get',
        params: query
    })
}

// 查询【请填写功能名称】详细
export function getAuth(id) {
    return request({
        url: '/system/auth/' + id,
        method: 'get'
    })
}

// 新增【请填写功能名称】
export function addAuth(data) {
    return request({
        url: '/system/auth/add',
        method: 'post',
        data: data
    })
}

// 修改【请填写功能名称】
export function updateAuth(data) {
    return request({
        url: '/system/auth',
        method: 'put',
        data: data
    })
}

// 删除【请填写功能名称】
export function delAuth(id) {
    return request({
        url: '/system/auth/' + id,
        method: 'delete'
    })
}

// 导出【请填写功能名称】
export function exportAuth(query) {
    return request({
        url: '/system/auth/export',
        method: 'get',
        params: query
    })
}
