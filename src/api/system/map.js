import request from '@/utils/request'

// Request the data to be traced【请求描点的数据】
export function requestDataOfDrawPoint(query) {
  // console.log(window.sessionStorage.getItem("myToken"))
  return request({
    url: '/system/adress/list',
    method: 'get',
    params:  query,
    headers: {
      'Authorization': window.sessionStorage.getItem("myToken")
    }
  })
}


export function RequestUploadVisitPersonInformation(data) {
  return request({
    url: '/system/adress/edit',
    method: 'post',
    data: data
  })
}



// requestTrendChartsData【请求拜访趋势图数据！】
export function requestTrendChartsData(query) {
  return request({
    url: '/system/adress/getAddVisitList',
    method: 'get',
    params: query,
    headers: {
      'Authorization': window.sessionStorage.getItem("myToken")
    }
  })
}

//
export function uploadNewCustomerInfo(data) {
  return request({
    url: '/system/adress/add',
    method: 'post',
    data: data,
    headers: {
      'Authorization': window.sessionStorage.getItem("myToken")
    }
  })
}










