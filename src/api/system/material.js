import request from '@/utils/request'

// 查询项目材料信息列表
export function listMaterial(query) {
  return request({
    url: 'project/material/list',
    method: 'get',
    params: query,
    headers: {
      'Authorization': window.sessionStorage.getItem("myToken")
    }
  })
}

// 查询项目材料信息详细
export function getMaterial(id) {
  return request({
    url: 'project/material/query',
    method: 'get',
    params: {
      id: id
    }
  })
}

// 查询项目信息详细
export function getDetails(id) {
  return request({
    url: '/project/details/query',
    method: 'get',
    params: {
      id
    }
  })
}
// 新增项目材料信息
export function addMaterial(data) {
  return request({
    url: '/system/material',
    method: 'post',
    data: data
  })
}

// 修改项目材料信息
export function updateMaterial(data) {
  return request({
    url: '/system/material',
    method: 'put',
    data: data
  })
}

// 删除项目材料信息
export function delMaterial(id) {
  return request({
    url: '/system/material/' + id,
    method: 'delete'
  })
}

// 导出项目材料信息
export function exportMaterial(query) {
  return request({
    url: '/system/material/export',
    method: 'get',
    params: query
  })
}
