import request from '@/utils/request'

// 查询项目信息列表
export function listDetails(query) {
  // console.log(window.sessionStorage.getItem("myToken"))
  return request({
    url: '/project/details/list ',
    method: 'get',
    params: query,
    headers: {
      'Authorization': window.sessionStorage.getItem("myToken")
    }
  })
}

// 查询项目信息详细
export function getDetails(id) {
  return request({
    url: '/project/details/query',
    method: 'get',
    params: {
      id: id
    }
  })
}

// 新增项目信息
export function addDetails(data) {
  return request({
    url: '/system/details',
    method: 'post',
    data: data
  })
}

// 修改项目信息
export function updateDetails(data) {
  return request({
    url: '/system/details',
    method: 'put',
    data: data
  })
}

// 删除项目信息
export function delDetails(id) {
  return request({
    url: '/system/details/' + id,
    method: 'delete'
  })
}

// 导出项目信息
export function exportDetails(query) {
  return request({
    url: '/system/details/export',
    method: 'get',
    params: query
  })
}
