## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

打包时，为了直观地发现项目中存在的问题，可以在打包时生成报告。生成报告的方式有两种：
1. 通过命令行参数的形式生成报告
`vue-cli-service build--report`
![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210813184355710-270905885.png)
修改工程配置文件 package.json，然后运行 npm run build 就可以在 dist 发布目录生成 report.html  
就是在你的package.json的文件的scripts里加上下面一句就可以了!
```json
scripts: {
    "build": "vue-cli-service build --mode prod --report",
}
```
2. 通过可视化的UI面板直接查看报告（推荐）
在可视化的UI面板中，通过控制台和分析面板，可以方便地看到项目中所存在的问题。
![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210814093006837-593857041.png)

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210814095539365-1358269397.png)

# 1.项目优化

## 1.1 项目优化策略

## 2. 通过vue.config.js修改webpack的默认配置

通过vue-cli 3.0工具生成的项目，<mark>默认隐藏了所有webpack的配置项</mark>，目的是为了屏蔽项目的配置过程，让程序员把工作的重心，放到具体功能和业务逻辑的实现上。

如果程序员有修改webpack默认配置的需求，可以在项目根目录中，按需创建vue.configjs这个配置文件，从而对项目的打包发布过程做自定义的配置（具体配置参考https://cli.vuejs.org/zh/config/#vue-config-js）

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210814094845627-1858547738.png)


### 3. 为开发模式与发布模式指定不同的打包入口

默认情况下，Vue项目的<mark>开发模式</mark>与<mark>发布模式</mark>，共用同一个打包的入口文件（即<mark>src/main.js</mark>）。为了将项目的开发过程与发布过程分离，我们可以为两种模式，各自指定打包的入口文件，即：
:qatar:1 开发模式的入口文件为<mark>src/main-dev.js</mark>
:qatar:2 发布模式的入口文件为<mark>src/main-prod.js</mark>



### 4. configureWebpack 和 chainWebpack



在vue.config.js导出的配置对象中，新增configureWebpack或chainWebpack节点，来自定义webpack的打包配置。

在这里，configureWebpack和chainVebpack的作用相同，唯一的区别就是它们修改webpack配置的方式不同：



① chainWebpack通过链式编程的形式，来修改默认的webpack配置
②configureWebpack通过操作对象的形式，来修改默认的webpack配置



两者具体的使用差异，可参考如下网址：
https://cli.vuejs.org/zh/guide/webpack.html#webpack-%E7%9B%B89%E5%85%B3



### 5.  通过 chainWebpack 自定义打包入口

代码示例如下：

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210814101124760-1143308695.png)

```js
module.exports = {
    chainWebpack: config => {
         config.when(process.env.NODE_ENV === 'production', config => {
          config.entry('app').clear().add('./src/main-prod.js')
        })

        config.when(process.env.NODE_ENV === 'development', config => {
          config.entry('app').clear().add('./src/main-dev.js')
        })
    }
}
```



### 6. 通过externals 加载外部 CDN资源

默认情况下，通过import 语法导入的第三方依赖包，最终会被打包合并到同一个文件中，从而导致打包成功k后，单文件体积过大的问题。



为了解决上述问题，可以通过 webpack的externals节点，来配置并加载外部的CDN资源。凡是声明在externals中的第三方依赖包，都不会被打包。

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210814114902962-1169672811.png)

同时，需要在public/index.html文件的头部，添加如下的CDN资源引用：



![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815102156503-149471398.png)



同时，需要在public/index.html文件的头部，添加如下的CDN资源引用：



<script src="https://cdn.bootcdn.net/ajax/libs/js-cookie/latest/js.cookie.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/element-ui/2.15.3/index.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.1.2/echarts.common.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/echarts/5.1.2/echarts.common.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/vue-router/3.0.1/vue-router.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/axios/0.18.0/axios.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/quill/1.3.7/quill.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/js-beautify/1.13.0/beautify.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/core-js/3.8.1/minified.min.js"></script>

<script src="https://cdn.bootcdn.net/ajax/libs/Sortable/1.10.2/Sortable.min.js"></script>



### 7. 通过CDN优化ElementUI的打包



虽然在开发阶段，我们启用了element-ui组件的按需加载，尽可能的减少了打包的体积，但是那些被按需加载的组件，还是占用了较大的文件体积。此时，我们可以将element-ui中的组件，也通过CDN的形式来加载，这样能够进一步减小打包后的文件体积。
具体操作流程如下：

①在main-prod.js中，注释掉element-ui按需加载的代码
②在index.html的头部域中，通过CDN加载 element-ui的js和css样式

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815120514325-384212758.png)

```html
<!-- element-ui 的 js 文件 -->
<script src="https://cdn.staticfile.org/element-ui/2.8.2/index.js"></script>

<!-- element-ui 的样式表文件 -->
<link rel="stylesheet" href="https://cdn.staticfile.org/element-ui/2.8.2/theme-chalk/index.css" />
```



### 8.  首页内容定制

不同的打包环境下，首页内容可能会有所不同。我们可以通过插件的方式进行定制，插件配置如下：

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815125331279-312606702.png)

在public/index.html首页中，可以根据isProd的值，来决定如何渲染页面结构：

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815132436472-1736849701.png)





### 9. 路由懒加载

当打包构建项目时，JavaScript包会变得非常大，影响页面加载。如果我们能把不同路由对应的组件分割成不同的代码块，然后当路由被访问的时候才加载对应组件，这样就更加高效了。

具体需要3步：
①安装@babel/plugin-syntax-dynamic-import包。`npm install --save-dev @babel/plugin-syntax-dynamic-import`
②在babel.config.js 配置文件中声明该插件。

```js
plugins: [
  '@babel/plugin-syntax-dynamic-import'
]
```

③将路由改为按需加载的形式，示例代码如下：

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815135131828-438899646.png)

关于路由懒加载的详细文档，可参考如下链接：


https://router.vuejs.org/zh/guide/advanced/lazy-loading.html

![](https://img2020.cnblogs.com/blog/2031306/202108/2031306-20210815145843403-1808544298.png)

































































































































































































































































